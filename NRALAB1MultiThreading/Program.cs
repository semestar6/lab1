﻿using System;
using System.Collections.Generic;

namespace NRALAB1MultiThreading
{
    class Program
    {

        static void Main()
        {
            try
            {
                var ulrs = new List<string>
                {
                    "http://econpy.pythonanywhere.com/ex/001.html",
                    "http://econpy.pythonanywhere.com/ex/002.html",
                    "http://econpy.pythonanywhere.com/ex/003.html",
                    "http://econpy.pythonanywhere.com/ex/004.html",
                    "http://econpy.pythonanywhere.com/ex/005.html",
                    "https://www.amazon.ca/DADAWEN-Canvas-Lace-up-Oxford-Shoes-Black/dp/B013YBWV1W/ref=sr_1_1?ie=UTF8&qid=1458158940&sr=8-1&keywords=shoes",
                    "https://www.amazon.ca/DADAWEN-Leather-Lace-up-Bussiness-shoes-Black/dp/B00Y2HNSNI/ref=sr_1_2?ie=UTF8&qid=1458158940&sr=8-2&keywords=shoes",
                    "https://www.amazon.ca/Gleader-Casual-High-top-Waterproof-Sneakers/dp/B00X9GBXTE/ref=sr_1_3?ie=UTF8&qid=1458158940&sr=8-3&keywords=shoes",
                    "http://www.foxsports.com/nba/story/wizards-kevin-seraphin-fined-15000-by-nba-031315",
                    "http://www.foxsports.com/nba/story/noah-returns-for-bulls-jefferson-zeller-out-for-hornets-031315",
                    "http://www.foxsports.com/nba/story/raptors-valanciunas-gets-night-off-after-birth-of-son-031315"
                };

                var manger = new ResourceManager(ulrs);
                manger.DownloadResourceParallel(12);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($"Error: {ex}");
            }
            finally
            {
                Console.ResetColor();
                Console.ReadKey();
            }

        }
    }
}
