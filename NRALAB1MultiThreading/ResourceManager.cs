﻿// ////////////////////////////////////////////////////////////////////////////
//       Author: Jurica Penjguisc
//       Created: 21.03.2017
//       Project : NRALAB1MultiThreading
//              
//       Property of: FESB  
//       @Confidential
// 
// //////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace NRALAB1MultiThreading
{
    public class ResourceManager
    {
        private readonly IList<string> _urls;

        public ResourceManager(IList<string> urls)
        {
            if (urls == null)
                throw new ArgumentNullException($"Argument {nameof(urls)} can not be null. \nPlease enter valid urls");
            _urls = urls;
        }

        public void DownloadResourceSequentially()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            try
            {
                foreach (var url in _urls)
                {
                    GetResource(url);
                }
                watch.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($"Error: {ex}");
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\n \nSequentially parsing last approximately: {watch.Elapsed.TotalSeconds} seconds.");
                Console.ResetColor();
            }
        }

        public void DownloadResourceParallel(int maxConnections)
        {
            System.Diagnostics.Stopwatch watch = null;
            try
            {
                var pool = new List<Thread>();
                var unhandledUrls = new Queue<string>(_urls);

                watch = System.Diagnostics.Stopwatch.StartNew();
                while (unhandledUrls.Count != 0)
                {
                    for (var i = 0; i < maxConnections; i++)
                    {
                        pool.Add(new Thread(GetResource));
                        pool[i].Start(unhandledUrls.Dequeue());
                        if (unhandledUrls.Count == 0)
                            break;
                    }
                    // Wait untile thread are finished
                    pool.ForEach(x => x.Join());

                    pool = new List<Thread>();
                }
                watch.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($"Error: {ex}");
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\n \nParallel parsing last approximately: {watch?.Elapsed.TotalSeconds} seconds.");
                Console.ResetColor();
            }
        }

        private void GetResource(object url)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write($"Starting to download: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write($"[{Thread.CurrentThread.ManagedThreadId}]                           :");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write($"{(string) url}\n");
            Console.ResetColor();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create((string)url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            // ReSharper disable once AssignNullToNotNullAttribute
            using (StreamReader reader = new StreamReader(stream))
            {
                var html = reader.ReadToEnd();
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write($"Finshed  to download: ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write($"[{Thread.CurrentThread.ManagedThreadId}]     -bytes read : {html.Length}    :");
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.Write($"{(string) url} \n");
                Console.ResetColor();
            }
        }
    }
}